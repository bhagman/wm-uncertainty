/*
||
|| @author         Brett Hagman <bhagman@roguerobotics.com>
|| @url            http://roguerobotics.com/
|| @contributor    Hugh Elliott <hugh.elliott@wondermakr.com>
||
|| @description
|| | This sketch controls a stepper motor, and a laser diode.  The stepper is mechanically
|| | connected to the dial control for a single slit aperture.  As a dial is turned, the laser
|| | diode turns on and the aperture opens and closes depending on the position of the dial.
|| | If left untouched for 15 seconds, the aperture resets to a random position, and the laser
|| | diode is turned off.
|| |
|| #
||
|| @notes
|| | On boot, the stepper closes the aperture until the limit switch is activated.  If the
|| | limit switch is ever activated during a close maneouver, the new point is considered
|| | the closed position (i.e. in case there is any slippage with the stepper shaft collar).
|| |
|| #
||
|| @license Please see License.txt for this project.
||
*/

#include <Wire.h>
#include <SmartDial.h>
#include <Stepper.h>

#define VERSIONSTR "1.3"

#define SDADDR 0x09

#define LASERPIN A1
#define MAXLASERONTIME (20 * 60000L)

#define AIN2 4
#define AIN1 5
#define BIN1 6
#define BIN2 7

// Aperture control has values from 0 to MAXPOSITION
#define STEPS_PER_REV 200
#define STEPPER_SPEED 60

#define STEPS_PER_DIAL_DETENT 3

#define TIMEOUT (15000L)

#define RESET_DIAL_POSITION 15
#define MAXPOSITION 20

// Set the direction of dial movement
#define DIRECTION 1

#define STOPPIN 9

SmartDial smartDial = SmartDial(SDADDR);

Stepper stepper(STEPS_PER_REV, 4, 5, 6, 7);

uint32_t lastTurnTime = 0;
uint32_t laserOnTime = 0;
int32_t lastPosition = 0;
int32_t position = 0;
bool awake = false;

// If we ever hit the stop switch, we reset the zero point, and the dial
bool moveStepper(int16_t steps)
{
  if (steps == 0)
    return false;

  int8_t direction = steps > 0 ? 1 : -1;

  steps = abs(steps);

  for (int i = 0; i < steps; i++)
  {
    if (digitalRead(STOPPIN) == HIGH && direction == -1)
    {
      smartDial.setPosition(0);
      return false;
    }
    else
    {
      // Use this to translate the direction of the stepper
      int8_t stepperDir = direction == 1 ? -1 : 1;
      stepper.step(stepperDir * STEPS_PER_DIAL_DETENT);
    }
  }
  return true;
}


void initializePosition()
{
  // close the aperture until we hit the limit switch
  uint8_t stepCount = 0;

  Serial.println(F("Resetting position."));
  while (stepCount < MAXPOSITION && moveStepper(-1))
  {
    Serial.println(F("Moving..."));
    stepCount++;
  }

  if (stepCount < MAXPOSITION)
  {
    Serial.println(F("Done. Dial set to 0."));
  }
  else
  {
    Serial.println(F("FAILURE! Halting. Couldn't find limit switch."));
    for (;;);
  }
}


int32_t getSmartDialPos()
{
  return smartDial.getPosition() * DIRECTION;
}


void setSmartDialPos(int32_t newPosition)
{
  smartDial.setPosition(newPosition * DIRECTION);
}


void resetPosition()
{
  int32_t stepsToMove;

  Serial.println(F("Resetting"));

  if (position < 0)
  {
    // there was some sort of overshoot, set current positon to 0
    setSmartDialPos(0);
    position = 0;
  }

  stepsToMove = RESET_DIAL_POSITION - position;

  setSmartDialPos(RESET_DIAL_POSITION); //smartDial.setPosition(RESET_DIAL_POSITION);
  Serial.print(F("Reset Move: "));
  Serial.print(position, DEC);
  Serial.print(F(" to "));
  Serial.print(RESET_DIAL_POSITION);
  Serial.print(F(" : "));
  Serial.println(stepsToMove, DEC);

  moveStepper(stepsToMove);
  lastPosition = RESET_DIAL_POSITION;
}


void setup()
{
  digitalWrite(SDA, HIGH);
  digitalWrite(SCL, HIGH);
  pinMode(LASERPIN, OUTPUT);
  digitalWrite(LASERPIN, LOW);
  pinMode(STOPPIN, INPUT);
  digitalWrite(STOPPIN, HIGH); // pull-up

  // Prepare the stepper motor control pins
  for (int i = 4; i < 8; i++)
  {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }

  Serial.begin(115200);

  Serial.println(F("Uncertainty V" VERSIONSTR));

  delay(500);  // Give everyone time to settle down

  if (smartDial.getVersion() == 0)
  {
    // smartDial missing or not at specified address

    Serial.print(F("Halting. SmartDial not at address: 0x"));
    Serial.println(SDADDR, HEX);

    for (;;);
  }

  stepper.setSpeed(STEPPER_SPEED);

  smartDial.reset();

  initializePosition();
  resetPosition();
  // Setup complete!
}


void loop()
{
  int32_t stepsToMove;

  position = getSmartDialPos(); // smartDial.getPosition();

  if (position > MAXPOSITION)
  {
    // if we've hit the maximum open position, we can move no more.
    setSmartDialPos(MAXPOSITION);
    position = MAXPOSITION;
  }

  if (position != lastPosition)
  {
    Serial.println(position);
    if (!awake)
    {
      awake = true;
      digitalWrite(LASERPIN, HIGH);
    }

    laserOnTime = millis();

    stepsToMove = position - lastPosition;

    if (moveStepper(stepsToMove) == false)
    {
      // we hit the limit switch
      position = 0;
      setSmartDialPos(0);
    }

    Serial.print(F("Moved: "));
    Serial.print(lastPosition, DEC);
    Serial.print(F(" to "));
    Serial.print(position, DEC);
    Serial.print(F(" : "));
    Serial.println(stepsToMove, DEC);

    lastPosition = position;

    lastTurnTime = millis();
  }

  if (awake && (position != RESET_DIAL_POSITION) && (millis() - lastTurnTime) > TIMEOUT)
  {
    resetPosition();
  }

  if (awake && (millis() - laserOnTime) > MAXLASERONTIME)
  {
    // Turn off laser, reset position.
    digitalWrite(LASERPIN, LOW);
    awake = false;
  }
}

